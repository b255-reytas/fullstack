let collection = [];

// Write the queue functions below.


module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};

function enqueue(item) {
  collection.push(item);
  return collection
}

function print() {
  return(collection);
}

function dequeue(item) {
  collection.shift();
  return collection
}

function front() {
  return collection[0];
}

function size() {
  return collection.length;
}

function isEmpty() {
  return collection.length === 0;
}