/*import { Row, Col, Card, Button } from 'react-bootstrap';

export default function CourseCard(){
	return(
        <Card className="cardHighlight p-3">
            <Card.Body>
                <Card.Title>
                    <h2>Structured Query Language (SQL)</h2>
                </Card.Title>
                <h5>Description:</h5>
                <Card.Text>
                	Structured Query Language (SQL) is a standardized programming language that is used to manage relational databases and perform various operations on the data in them. Initially created in the 1970s, SQL is regularly used not only by database administrators, but also by developers writing data integration scripts and data analysts looking to set up and run analytical queries.
                </Card.Text>
                <h5>Price:</h5>
				<Card.Text>Php 50,000</Card.Text>
            </Card.Body>
        </Card>
	)
}*/


import { Card, Button } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';


export default function CourseCard({courseProp}) {
    // Checks to see if the data was successfully passed
    // console.log(props);
    // Every component receives information in a form of an object
    // console.log(typeof props);

    // Use the state hook for this component to be able to store its state
    // States are used to keep track of information related to individual components

    const [count, setCount] = useState(0);
    const [seats, setSeats] = useState(30);
    // Using the state hook returns an array with the first element being a value and the second element as a function that used to change the value of the first element

    console.log(useState(0));

    // Function that keeps track of the enrollees for a course
    // By default JavaScript is synchronous it executes code from the top of the file all the way down 
    // The setter function for useStates are asychronous allowing it to execute seperately from other codes in the program
    // The "setCount function is being executed while the "console.log" is already completed

/*    function enroll(){
        setCount(count + 1);
        console.log('Enrollees:' + count)
        setSeats(seats - 1);
        console.log('seats' + count)
    }
    // Define a "useEffect" hook to have the "CourseCard" component perform a certain task after every DOM update
    // This is run automatically after initial render and for every DOM update
    // Checking for the availability for enrollment of a course is better suited here
    // React will re-run this effect ONLY if any of the values contained in this array has changed from the last render/update
    useEffect(() => {
        if (seats === 0){
            // setIsOpen(false);
        }
    }, [seats])*/

    const {name, description, price, _id} = courseProp

    return (
        <Card>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>PhP {price}</Card.Text>
                <Card.Text>Enrollees: {count}</Card.Text>
                <Card.Text>Seats: {seats}</Card.Text>
                {/*{<Button className = "bg-primary" onClick = {enroll}>Enroll</Button>}*/}
                <Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
            </Card.Body>
        </Card>
    )
}

CourseCard.propTypes = {
    // The shape method is used to check if a prop object conforms to a specific shape
    course: PropTypes.shape({
        // Define the properties and their expected types
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired
    })
}