import React from 'react';
import { Link } from 'react-router-dom'

function NotFound(){
	return(
		<>
		<h1>Page Not Found</h1>
		<p>Go back to the <Link to="/">homepage</Link></p>
		</>
	)
}

export default NotFound;